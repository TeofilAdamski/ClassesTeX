% Classe pour les articles
%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{artintin}

\LoadClass{tintin}

% Titre et sommaire

\renewcommand{\maketitle}{%
	\mainmatter
	\thispagestyle{plain}
	\pagestyle{headers}
	\begin{center}
		{\Large\sbweight\@titre}
		\vskip1em
		Téofil \bsc{Adamski}
		\vskip1em
		{\footnotesize\ttfamily\@date}
	\end{center}
	\vskip2em
}

\RequirePackage[useregional, showdow]{datetime2}
\date{Dernière édition le \today{} à \DTMcurrenttime}

\renewcommand{\tableofcontents}{
	\section*{Sommaire}
	\@starttoc{toc}
	\vskip5em
}

\titlecontents{section}[2em]
	{}
	{\contentslabel[\thecontentslabel]{2em}}
	{}
	{\ \titlerule*[6pt]{.}\contentspage}
\titlecontents{subsection}[4em]
	{}
	{\contentslabel[\thecontentslabel]{2em}}
	{}
	{\ \titlerule*[6pt]{.}\contentspage}


% En-têtes et marges

\fancypagestyle{headers}{
	\fancyhead{}
	\fancyhead[c]{\small\scshape\@titre}
	\fancyfoot{}
	\fancyfoot[c]{\small\thepage}
}

\fancypagestyle{plain}{
	\fancyhead{}
	\fancyfoot{}
	\fancyfoot[c]{\small\thepage}
}

\geometry{margin=3.5cm}

% Sections et numérotations

\titleformat{\section}
	{\centering\sbweight}
	{\@nosection{\thesection.}}
	{\z@}
	{#1}
\titleformat{\subsection}
	{\sbweight}
	{\@nosection{\thesubsection.}}
	{\z@}
	{#1}
\titleformat{\subsubsection}
	{\itshape}
	{\@nosection{\thesubsubsection.}}
	{\z@}
	{#1}

\renewcommand{\thesection}{\arabic{section}}
\renewcommand{\thedefinition}{\thesection.\arabic{definition}}
\@addtoreset{definition}{section}
\renewcommand{\theequation}{\thesection.\arabic{equation}}
\@addtoreset{equation}{section}

% Résumé

\newenvironment{resume}{
	\list{}{\leftmargin1cm\rightmargin\leftmargin}
	\item\footnotesize\textsb{Résumé.}\quad%
}{
	\endlist\vskip2em
}
