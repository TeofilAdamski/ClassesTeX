% Classe pour les documents généraux
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{tintin}

% --- Options ---

\newif\ifnotitle\notitlefalse
\DeclareOption{notitle}{\notitletrue}

\newif\ifnotoc\notocfalse
\DeclareOption{notoc}{\notoctrue}

\ProcessOptions

% --- Chargement de la classe ---

\LoadClass[a4paper, twoside, 10pt]{book}

% --- Packages ---

\RequirePackage[french]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage[margin=3cm]{geometry}
\RequirePackage{lmodern}
\RequirePackage[rm={oldstyle=true}]{cfr-lm}
\RequirePackage{microtype}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{amsthm}
\RequirePackage{array}
\RequirePackage{mathtools}
\RequirePackage{fancyhdr}
\RequirePackage{mathrsfs}
\RequirePackage{stmaryrd}
\RequirePackage[usenames, dvipsnames, table]{xcolor}
\RequirePackage{graphicx}
\RequirePackage[inline]{enumitem}
\RequirePackage{xspace}
\RequirePackage{tikz}
\RequirePackage{tikz-cd}
\RequirePackage[explicit]{titlesec}
\RequirePackage{titletoc}
\RequirePackage[hypertexnames=false]{hyperref}

% --- Couleurs des liens et information sur le PDF ---

\AtBeginDocument{\hypersetup{
	pdftitle={\@titre},
	pdfauthor={Téofil Adamski}
}}

% --- En-tête et pied-de-page ---

\renewcommand{\headrulewidth}{\z@}

\renewcommand{\chaptermark}[1]{\markboth{\chaptername\ \thechapter.\ #1}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection.\ #1}}

\fancypagestyle{headers}{
	\fancyhead{}
	\fancyhead[LO]{\small\scshape\leftmark}
	\fancyhead[RO, LE]{\small\thepage}
	\fancyhead[RE]{\small\scshape\rightmark}
	\fancyfoot{}
}

\fancypagestyle{plain}{
	\fancyhead{}
	\fancyhead[RO, LE]{\small\thepage}
	\fancyfoot{}
}

% --- Titre ---

\newcommand{\titre}[1]{\gdef\@titre{#1}}
\newcommand{\nochapitre}[1]{\gdef\@nochapitre{#1}}
\nochapitre{}

\renewcommand{\maketitle}{%
	\clearpage
	\thispagestyle{plain}
	\ifnotitle\else
		\if\@nochapitre\@empty\relax
			\chapter*{\@titre}
		\else
			\setcounter{chapter}{\@nochapitre-1}
			\chapter{\@titre}
		\fi
	\fi
}
\AtBeginDocument{\ifnotitle\pagestyle{plain}\else\maketitle\pagestyle{headers}\fi}

% --- Table des matières ---

\setcounter{tocdepth}{2}

\titlecontents{part}[0pt]
	{\addvspace{3ex}\sbweight\itshape Partie }
	{\thecontentslabel{}.}
	{}
	{}
\titlecontents{chapter}[0pt]
	{\addvspace{1ex}\sbweight}
	{\thecontentslabel{}\quad}
	{}
	{}

\titlecontents{lsection}[2em]
	{\footnotesize}
	{\contentslabel[\thecontentslabel]{2em}}
	{}
	{\ \titlerule*[6pt]{.}\contentspage}
\titlecontents{lsubsection}[5em]
	{\footnotesize}
	{\contentslabel[\thecontentslabel]{3em}}
	{}
	{\ \titlerule*[6pt]{.}\contentspage}

% --- Commande d’espacement ---

\newskip\EspacementPar
\EspacementPar10pt plus .2ex minus .2ex\relax
\titlespacing{\paragraph}{\z@}{\EspacementPar}{1em}

\newcommand{\Saut}{\addvspace\EspacementPar}

% --- Style des sections ---

% Parties
\renewcommand\part{%
    \cleardoublepage
  \thispagestyle{plain}%
  \if@twocolumn
    \onecolumn
    \@tempswatrue
  \else
    \@tempswafalse
  \fi
  \null\vfil
  \secdef\@part\@spart}

\def\@part[#1]#2{%
    \ifnum \c@secnumdepth >-2\relax
      \refstepcounter{part}%
      \addcontentsline{toc}{part}{\thepart\hspace{1em}#1}%
    \else
      \addcontentsline{toc}{part}{#1}%
    \fi
    \markboth{}{}%
    {\centering
     \interlinepenalty \@M
     \normalfont
     \ifnum \c@secnumdepth >-2\relax
       \huge\partname\nobreakspace\thepart
       \par
       \vskip 20\p@
     \fi
     \Huge \sbweight\MakeUppercase{#2}\par}%
    \@endpart}
\def\@spart#1{%
    {\centering
     \interlinepenalty \@M
     \normalfont
     \Huge \bfseries\MakeUppercase{#1}\par}%
    \@endpart}

% Chapitres
\newif\iftoc\toctrue

\renewcommand\chapter{%
	\cleardoublepage
	\thispagestyle{plain}%
	\global\@topnum\z@
	\@afterindentfalse
	\secdef\@chapter\@schapter
}
\def\@makechapterhead#1{%
  \null\vskip3em
  {\parindent \z@ \normalfont
    \ifnum \c@secnumdepth >\m@ne
      \if@mainmatter
        {\LARGE\@chapapp\space \thechapter}
        \vskip1em\nobreak
      \fi
    \fi
    \interlinepenalty\@M
    {\itshape\huge#1}\par\nobreak
    \ifnotoc\else
		\vskip2em
		\list{}{\listparindent0pt%
				\leftmargin4cm
				\rightmargin0pt
				\parsep0pt}%
		\item\relax
		\iftoc
			\regle{.4pt}\hskip-3.25pt\par%
			\startcontents%
			\printcontents{l}{1}[2]{}%
			\regle{.4pt}%
		\fi
		\endlist
	\fi
    \vskip6em
  }}
\def\@makeschapterhead#1{%
  {\parindent \z@ \raggedright
    \normalfont
    \interlinepenalty\@M
    {\itshape\huge#1}\par\nobreak
    \ifnotoc\else
		\vskip2em
		\list{}{\listparindent0pt%
				\leftmargin4cm
				\rightmargin0pt
				\parsep0pt}%
		\item\relax
		\iftoc
			\regle{.4pt}\hskip-3.25pt\par%
			\startcontents
			\printcontents{l}{1}{\setcounter{tocdepth}{2}}%
			\regle{.4pt}%
		\fi
		\endlist
	\fi
    \vskip6em
  }}

% Sections, sous-sections, etc
\newcommand{\@nosection}[1]{\llap{#1\enskip}}

\titleformat{\section}
	{\sbweight\Large}
	{\@nosection{\thesection.}}
	{\z@}
	{#1}
\titleformat{\subsection}
	{\sbweight\large}
	{\@nosection{\thesubsection.}}
	{\z@}
	{#1}
\titleformat{\subsubsection}
	{\sbweight\itshape}
	{\@nosection{\thesubsubsection.}}
	{\z@}
	{#1}

\titleformat{\paragraph}[runin]{\sbweight}{}{1em}{#1.}
\titlespacing{\paragraph}{0pt}{\EspacementPar}{1em}
\titleformat{\subparagraph}[runin]{\itshape}{}{1em}{#1.}
\titlespacing{\subparagraph}{0pt}{1ex}{1em}

\renewcommand{\thesection}{\thechapter.\arabic{section}}
\renewcommand{\thesubsubsection}{\thesubsection.\arabic{subsubsection}}
\setcounter{secnumdepth}{3}
\renewcommand{\theequation}{\thechapter.\arabic{equation}}
\renewcommand{\thefootnote}{(\arabic{footnote})}

% --- Indice en romain ---

{\catcode`"=\active
 \gdef"#1{_\text{#1}}}
\mathcode`"="8000

% --- Personnalisation des environnements maths display ---

\allowdisplaybreaks[4]

\AtBeginDocument{
	\abovedisplayskip=5pt plus 2pt minus 2pt
	\belowdisplayskip=5pt plus 2pt minus 2pt
}

% --- Personnalisation des polices ---

\newif\ifbf\bffalse
\DeclareRobustCommand*{\sbweight}{%
	\bftrue
	\not@math@alphabet\bfseries\mathbf%
	\fontseries\sbdefault\selectfont%
	\boldmath%
}

\DeclareRobustCommand*{\bsc}[1]{\leavevmode\begingroup\kern0pt\scshape%
	{\ifbf\bfseries\fontfamily{cmr}\selectfont\fi #1}\endgroup%
}

\SetSymbolFont{stmry}{bold}{U}{stmry}{m}{n}

% --- Éléments de formatage ---

\setlist[enumerate]{itemsep=0pt}
\setlist[enumerate, 1]{listparindent=\parindent}
\setlist[itemize, 1]{label=--, listparindent=\parindent}
\setlist[itemize, 2]{label=$\circ$}
\setlist[description]{font=\normalfont\itshape}

% --- Virgules ---

\AtBeginDocument{%
    \mathchardef\mathcomma\mathcode`\,
    \mathcode`\,="8000
}

{\catcode`,=13
    \gdef,{\futurelet\@let@token\sm@rtcomma}
}

\def\sm@rtcomma{%
    \ifx\@let@token\@sptoken\else%
    \ifx\@let@token\mathpunct\else%
    \mathord\fi\fi\mathcomma%
}

% --- Commandes perso ---

\RequirePackage{commandes}

\endinput
