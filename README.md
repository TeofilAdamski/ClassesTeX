# Packages et classes LaTeX

Mes packages et classes LaTeX personnels pour rédiger mes documents (`tintin`), articles (`artintintin`) et diaporamas (`diapo`) de maths.

## Installation

- Placer tout le contenu du dépôt dans le dossier `texmf`.
- Puis actualiser les fichiers avec la commande `texhash`.

## Utilisation

Voici un exemple pour utiliser la classe `tintin`.

```latex
\documentclass[10pt]{tintin}

\titre{Nom du document}
\nochapitre{1}

\begin{document}

\section{Première partie}

% …

\end{document}
```
